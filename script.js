$(document).ready(function() {
  const comboBox = document.getElementById('myComboBox');
  const checkButton = document.getElementById('checkButton');
  const cancelButton = document.getElementById('cancelButton');

  // Event listener saat combo box berubah nilai
  $(comboBox).on('change', function() {
    const selectedOption = $(this).val();
    if (selectedOption === '') {
      checkButton.classList.add('hidden');
      cancelButton.classList.add('hidden');
    } else {
      checkButton.classList.remove('hidden');
      cancelButton.classList.remove('hidden');
    }
  });

  // Event listener saat tombol centang diklik
  $(checkButton).on('click', function() {
    const selectedOption = $('#myComboBox').val();
    if (selectedOption === 'pilihan1') {
      alert('Pilihan 1 tersimpan!');
    } else if (selectedOption === 'pilihan2') {
      alert('Pilihan 2 tersimpan!');
    }
  });

  // Event listener saat tombol cancel diklik
  $(cancelButton).on('click', function() {
    alert('Batal memilih!');
  });
});
